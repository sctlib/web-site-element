# modular `web-site-element`

- web-component to fetch and render data, to build web pages
- netlify-cms configuration to manage the data with a GUI interface

It is a configuration to make a modular content management, and display system.

It should allow to create all sort of content, later rendered by their type.

## Devlopment usage

> You will need `node.js` installed (see `nvm` for installation method).

Run a local netlify-cms development server.
```
npx netlify-cms-proxy-server
```
It will commit changes to the local folders, if the `admin/options[config.local_backend]` is set to `true`.

## Production

To be able to use the CMS in production, on the live server, and make edits directly to the git repository at the git provider:

Update the file at:
```
admin/options[config.backend]
```

- docs: https://www.netlifycms.org/docs/backends-overview/#backend-configuration
