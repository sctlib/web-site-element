import {
	page,
	fetchPage,
	fetchPageSections,
	fetchEnv,
	fetchConfig,
	fetchRoutes,
	fetchLayout,
	fetchMenu,
	registerRoutes,
} from '../api.js'

class WebPage extends HTMLElement {
	static get observedAttributes() {
		return ['id']
	}

	get id() {
		return this.getAttribute('id')
	}

	attributeChangedCallback(name, oldValue, newValue) {
		if (name === 'id') {
			this.init()
		}
	}

	async connectedCallback() {
		this.$root = this
		this.init()
	}
	async init() {
		/* store previous page to compare layouts */
		const previousPage = this.page ? JSON.parse(JSON.stringify(this.page)) : {}
		const {layout: previousLayout} = previousPage

		/* fetch page model */
		if (this.id) {
			try {
				this.page = await fetchPage(this.id)
			} catch (e) {
				console.log('error fetching page data', e)
			}
		}
		const {layout: newLayout} = this.page || {}

		if (newLayout === previousLayout) {
			return this.renderPage()
		}

		if (newLayout) {
			try {
				this.layout = await fetchLayout(this.page.layout)
			} catch (e) {
				console.log('Error fetching layout data', e)
			}
		}
		this.renderLayout()
	}
	renderLayout() {
		this.$root.innerHTML = ''
		if (this.layout) {
			this.layout.slots.forEach(slot => {
				this.renderSlot(slot)
			})
		} else {
			this.renderSlot({type: 'page'})
		}
	}
	renderSlot(slot) {
		if (slot.type === 'page') {
			this.$page = document.createElement('slot')
			this.$page.setAttribute('name', 'page')
			this.$root.append(this.$page)
			this.renderPage()
		}

		if (slot.type === 'menu') {
			const $menu = document.createElement('web-menu')
			$menu.setAttribute('id', slot.menu)
			this.$root.append($menu)
		}

		if (slot.type === 'section') {
			const $section = document.createElement('web-section')
			$section.setAttribute('id', slot.section)
			this.$root.append($section)
		}
	}
	renderPage() {
		if (!this.$page) {
			this.$page = this.$root
		}
		this.$page.innerHTML = ''
		if (!this.page || !this.page.sections) {
			const $section = document.createElement('section')
			$section.innerText = `No section: ${this.id}`
			this.$page.append($section)
		} else {
			this.page.sections.forEach(section => {
				const $section = document.createElement('web-section')
				$section.setAttribute('id', section)
				this.$page.append($section)
			})
		}
	}
}

export default WebPage
