import {
	fetchLayout,
} from '../api.js'

class WebLayout extends HTMLElement {
	static get observedAttributes() {
		return ['layout', 'page']
	}
	get layout() {
		return this.getAttribute('layout')
	}
	get page() {
		return this.getAttribute('page')
	}
	attributeChangedCallback(name, oldVal, newVal) {
		if (name === 'page') {
			if (newVal) {
				this.querySelector('web-page').setAttribute('id', newVal)
			} else {
				this.querySelector('web-page').removeAttribute('id')
			}
		}
	}
	async connectedCallback() {
		this.init()
	}
	async init() {
		if (this.layout) {
			this.data = await fetchLayout(this.layout) || {}
		}

		if (!this.data || !this.data.slots) {
			this.data = {
				...this.data,
				slots: [{
					type: 'page',
				}]
			}
		}

		this.renderSlots()
	}
	renderSlots() {
		if (this.data.slots)
		this.data.slots.forEach(slot => {
			if (slot.type === 'page') {
				const $page = document.createElement('web-page')
				$page.setAttribute('id', this.page)
				this.append($page)
			}

			if (slot.type === 'menu') {
				const $menu = document.createElement('web-menu')
				$menu.setAttribute('id', slot.menu)
				this.append($menu)
			}

			if (slot.type === 'section') {
				const $section = document.createElement('web-section')
				$section.setAttribute('id', slot.section)
				this.append($section)
			}
		})
	}
}

export default WebLayout
