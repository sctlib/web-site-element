import {
	fetchEnv,
	fetchMenu,
} from '../api.js'

class WebMenu extends HTMLElement {
	static observedAttributes() {
		return ['id']
	}
	get id() {
		return this.getAttribute('id')
	}
	async connectedCallback() {
		try {
			this.env = await fetchEnv()
			this.menu = await fetchMenu(this.id)
		} catch (e) {
			console.log('Error fetching menu', this.id)
		}
		if (this.menu && this.menu.links && this.menu.links.length) {
			this.render()
		}
	}
	render() {
		const {origin} = window.location
		let {pathname} = this.env
		/* normalize path */
		if (pathname.startsWith('/')) {
			pathname = pathname.slice(1, pathname.length)
		}
		if (pathname.endsWith('/')) {
			pathname = pathname.slice(0, pathname.length - 1)
		}
		/* build menu and its links */
		const $menu = document.createElement('menu')
		this.menu.links.forEach(link => {
			const $li = document.createElement('li')
			const $anchor = document.createElement('a')
			$anchor.innerText = link.title

			if (link.url.startsWith('https://') || link.url.startsWith('http://')) {
				$anchor.href = link.url
			} else if (link.url === '/') {
				if (this.env.pathname === link.url || !this.env.pathname) {
					$anchor.href = `${origin}`
				} else {
					$anchor.href = `${origin}/${pathname}/`
				}
			} else {
				if (this.env.pathname === '/' || !this.env.pathname) {
					$anchor.href = `${origin}/${link.url}`
				} else {
					$anchor.href = `${origin}/${pathname}/${link.url}`
				}
			}

			$li.append($anchor)
			$menu.append($li)
		})
		this.append($menu)
	}
}

export default WebMenu
