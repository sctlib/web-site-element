import {
	fetchSection,
} from '../api.js'

class WebSection extends HTMLElement {
	static observedAttributes() {
		return ['id']
	}
	get id() {
		return this.getAttribute('id')
	}
	async connectedCallback() {
		if (this.id) {
			try {
				this.data = await fetchSection(this.id)
			} catch (e) {
				console.log('Error fetching section', this.id, e)
			}
		}
		if (this.data.blocks) {
			this.renderBlocks()
		} else {
			console.info('section with no blocks')
		}
	}
	renderBlocks() {
		this.innerHTML = ''
		this.data.blocks.forEach(block => {
			if (block.type === 'title') {
				const $block = document.createElement('h1')
				$block.innerText = block.content
				this.append($block)
			}
			if (block.type === 'link') {
				const $block = document.createElement('a')
				$block.href = block.url
				$block.innerText = block.url
				this.append($block)
			}
			if (block.type === 'content') {
				const $block = document.createElement('div')
				$block.innerText = block.content
				this.append($block)
			}
			if (block.type === 'image') {
				const $block = document.createElement('img')
				$block.src = `./${block.src}`
				this.append($block)
			}
		})
	}
}

export default WebSection
