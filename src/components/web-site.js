import {
	page,
	fetchEnv,
	fetchConfig,
	fetchLayout,
	fetchRoutes,
	fetchPage,
	registerRoutes,
} from '../api.js'

class WebSite extends HTMLElement {
	static observedAttributes() {
		return ['admin']
	}

	get admin() {
		return this.getAttribute('admin') === 'true'
	}

	async connectedCallback() {
		this.$root = this

		this.env = await fetchEnv()
		/* the base URL of the router */
		if (this.env.pathname && this.env.pathname.length > 1) {
			page.base(this.env.pathname)
		}

		/* the config of the site */
		this.config = await fetchConfig()
		const {layout, theme, routes} = this.config

		if (layout) {
			this.layout = await fetchLayout(layout)
			this.layout && this.renderLayout()
		}

		if (theme) {
			this.renderTheme(theme)
		}

		if (routes) {
			this.routes = await fetchRoutes(routes)
		}

		if (this.admin) {
			this.routes.push({
				uid: 'admin',
				slug: 'admin',
			})
		}
		/* register the routes in the router,
			 rendering is done by the router's route callback */
		registerRoutes(this.routes, this.renderRoute.bind(this))
	}

	async renderRoute(props) {
		if (props.routeData.uid === 'admin') {
			console.info('render admin?')
		}
		this.$page.setAttribute('id', props.routeData.uid)
	}

	renderLayout() {
		if (this.layout.slots)
			this.layout.slots.forEach(slot => {
				if (slot.type === 'page') {
					this.$page = document.createElement('web-page')
					this.$root.append(this.$page)
				}

				if (slot.type === 'menu') {
					const $menu = document.createElement('web-menu')
					$menu.setAttribute('id', slot.menu)
					this.$root.append($menu)
				}

				if (slot.type === 'section') {
					const $section = document.createElement('web-section')
					$section.setAttribute('id', slot.section)
					this.$root.append($section)
				}
			})
	}

	renderTheme(themeId) {
		const $style = document.createElement('link')
		$style.setAttribute('rel', 'stylesheet')

		if (themeId !== 'none') {
			$style.setAttribute('href', `./src/styles/${themeId}.css`)
		}

		$style.addEventListener('load', (event) => {
			this.setAttribute('loaded-theme', true)
		})
		this.$root.prepend($style)
	}
}

export default WebSite
