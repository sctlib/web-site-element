import page from 'https://cdn.jsdelivr.net/npm/page/page.mjs'

/*
	 router
 */
// https://github.com/visionmedia/page.js/issues/537
page.configure({window: window})

const registerRoutes = (routes, callback) => {
	page('*', (ctx, next) => {
		next()
	})
	/* register each routes from our router if any */
	routes.forEach((route) => {
		if (route.slug === 'index') {
			page(`/`, (ctx, next) => {
				ctx.routeData = route
				callback(ctx, next)
			})
		} else {
			let slug = route.slug
			page(`/${slug}`, (ctx, next) => {
				ctx.routeData = route
				callback(ctx, next)
			})
		}
	})
	page('*', (ctx, next) => {
		ctx.routeData = {
			uid: '404'
		}
		callback(ctx, next)
	})
	page(window.location)
}


/*
	 API to fetch local data managed by cms
 */
const fetchEnv = async () => {
	return fetchModel('.env') || {}
}
const fetchConfig = async () => {
	return fetchModel('content/config')
}
const fetchLayout = async (layoutUid) => {
	return fetchModel(`content/layouts/${layoutUid}`)
}
const fetchPage = async (pageUid) => {
	return fetchModel(`content/pages/${pageUid}`)
}
const fetchPageSections = async (sectionUids) => {
	const sectionPromises = sectionUids.map(uid => {
		return fetchSection(uid)
	})
	return Promise.all(sectionPromises)
}
const fetchSection = async (sectionUid) => {
	return fetchModel(`content/sections/${sectionUid}`)
}
const fetchRoutes = async (routes) => {
	const promises = routes.map(route => {
		return fetchModel(`content/pages/${route}`)
	})
	const routesData = await Promise.all(promises)
	const routesSerialized = routesData.map((route, index) => {
		return {
			...route,
			uid: routes[index]
		}
	})
	return routesSerialized
}
const fetchMenu = async (menuUid) => {
	return fetchModel(`content/menus/${menuUid}`)
}
const fetchModel = async(contentPath, extension = 'json') => {
	let data
	try {
		const res = await fetch(`./${contentPath}.${extension}`)
		data = await res.json()
		data._path = contentPath
	} catch (e) {
		console.log('Error fetching data', contentPath, e)
	}
	return data
}

export {
	fetchModel,
	fetchEnv,
	fetchConfig,
	fetchRoutes,
	fetchLayout,
	fetchPage,
	fetchPageSections,
	fetchSection,
	fetchMenu,
	page,
	registerRoutes,
}
