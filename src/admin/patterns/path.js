export default [
	'^[A-Za-z0-9]+(?:(-|\/|:)[A-Za-z0-9]*)*$',
	'This field must be only text and cannot have spaces. Only use dash ("-"), and forward slash ("/"), letters A to Z, a to z, and numbers'
]
