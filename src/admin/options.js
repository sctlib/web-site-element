import menus from './models/menus.js'
import pages from './models/pages.js'
import sections from './models/sections.js'
import layouts from './models/layouts.js'
import configs from './models/configs.js'

export default {
	config: {
		/* Skips config.yml.
			 By not skipping, the configs will be merged, with the js-version taking priority. */
		load_config_file: false,
		display_url: window.location.origin,

		/* only use when testing locally in dev mode, and makes changes to local files; `npx-netlify-cms-proxy-server` */
		local_backend: true,
		/* when site admin is live, and commits to gitlab */
		backend: {
			name: 'gitlab',
			branch: 'main',
			repo: 'sctlib/web-site-element',
			auth_type: 'pkce',
			app_id: 'd8152d2557c854b88986ab976671b93c9b7b8864f495e0ff5b3b507378415f85',
		},

		media_folder: 'public/media/images',
		public_folder: 'media/images',

		/* configuration for multilanguage
			 expects to persists files in `<folder>/<locale>/<slug>.<extension>
			 Uncomment i18n object, and remove the `/de` trailing path
			 of models configurations, when want to activate i18n;
			 the nextjs app system/content is already supporting it
		 */
		/* i18n: {
			 structure: 'multiple_folders',
			 locales: ['de', 'en'],
			 default_locale: 'de',
			 }, */

		slug: {
			encoding: 'ascii',
			clean_accents: true,
			sanitize_replacement: '-',
		},

		/* our data models */
		collections: [
			pages,
			sections,
			menus,
			layouts,
			configs,
		],
	}
}
