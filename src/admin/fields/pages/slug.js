import patternPath from '../../patterns/path.js'

export default {
	label: 'Slug',
	name: 'slug',
	widget: 'string',
	required: true,
	hint: 'The Slug is the text used to create the URL of this item: https://example.com/my-article ; where my-article is the slug. It is also the unique ID (unique IDentifier), of this content, used to get this exact item when being requested by the site layout. The slug should always match the content title. If the slug is equal to "index", the router will "strip" it from the url, so it is a clean "/"',
	pattern: patternPath
}
