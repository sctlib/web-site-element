/*
	 This content type uses netlify-cms beta feature:
	 https://www.netlifycms.org/docs/beta-features/#list-widget-variable-types
 */

const imageBlock = {
	label: "Image",
	name: "image",
	widget: "object",
	fields: [
		{
			label: "Title",
			name: "title",
			widget: "string",
			required: true,
		},
		{
			label: "Image Source",
			name: "src",
			widget: "image",
			required: true,
			media_library: { config: { multiple: false } },
		},
	],
}
const ctaBlock = {
	label: "Link",
	name: "link",
	widget: "object",
	fields: [
		{
			label: "Title",
			name: "title",
			widget: "string",
			required: true,
		},
		{
			label: "URL",
			name: "url",
			widget: "string",
		},
	],
}

const contentBlock = {
	label: "Content",
	name: "content",
	widget: "object",
	fields: [
		{
			name: "content",
			widget: "text",
			required: true,
		},
	],
}

const titleBlock = {
	label: "Title",
	name: "title",
	widget: "object",
	fields: [
		{
			name: "content",
			widget: "text",
			required: true,
		},
	],
}

const blockTypes = [
	titleBlock,
	imageBlock,
	ctaBlock,
	contentBlock,
]

export default {
	label: "Blocks",
	name: "blocks",
	widget: "list",
	types: blockTypes,
	hint: 'Blocks are the content elements (text, image, links etc.) composing a section. There can be many blocks in a section, but it is also good to organize blocks in different meaningully named section, depending on their position on the site, or their role.',
}
