import patternPath from '../../patterns/slug.js'

export default {
	label: 'ID (unique identifier)',
	name: 'id',
	widget: 'string',
	required: true,
	hint: 'The unique ID (unique IDentifier), of this content. It is used to get this exact item when being requested by the site layout, it should be unique in the list of all items of the same type. Examples: "my-id", "a-unique-id", "uniqueidofthiselement"; should always be lowercase, no special characters, words sperated with "-", no spaces, human readable (easily accessible when making content relations).',
	pattern: patternPath,
}
