/*
	 This content type uses netlify-cms beta feature:
	 https://www.netlifycms.org/docs/beta-features/#list-widget-variable-types
 */

const pageSlot = {
	label: "Page",
	name: "page",
	widget: "object",
	fields: [
		{
			label: "Page",
			name: "page",
			widget: "hidden",
			required: true,
			default: true,
		},
	],
}

const menuSlot = {
	label: "Menu",
	name: "menu",
	widget: "object",
	fields: [
		{
			name: 'menu',
			widget: 'relation',
			collection: 'menus',
			search_fields: ['id'],
			value_field: '{{slug}}',
			display_fields: ['id'],
			multiple: false,
			required: true,
		},
	],
}

const sectionSlot = {
	label: 'Section',
	name: 'section',
	widget: "object",
	fields: [
		{
			name: 'section',
			widget: 'relation',
			collection: 'sections',
			search_fields: ['id'],
			value_field: '{{slug}}',
			display_fields: ['id'],
			multiple: false,
			required: true,
		},
	],
}

const slotTypes = [
	menuSlot,
	pageSlot,
	sectionSlot,
]

export default {
	label: "Slots",
	name: "slots",
	widget: "list",
	types: slotTypes,
	hint: 'Slots are "positions" of elements in the layout. Organize slot in the list, to describe what elements are composing the layout, and in which "order" they appear (the exact styles of the layouts are defined by the "config.site.theme" selection). The "page" slot, is used to position where the page and its sections will be inserted.',
}
