const configs = {
	name: 'configs',
	label: 'Config',
	editor: {
		preview: false
	},
	description: 'Configuration options for all parts of the website.',
	files: [
		{
			name: "Site",
			label: 'Site',
			file: 'content/config.json',
			fields: [
				{
					label: 'Router',
					name: 'routes',
					hint: 'Select all pages, that will be used available in the router. Selected pages will be available to access through the url "https://example.org/slug" ; where "slug", is the slug of the page model.',
					widget: 'relation',
					collection: 'pages',
					search_fields: ['slug'],
					value_field: '{{slug}}',
					display_fields: ['slug'],
					multiple: true,
					required: false,
				},
				{
					label: 'Layout',
					name: 'layout',
					hint: 'The Site layout, is the layout used globally across all the pages of the site. It is always visible.',
					widget: 'relation',
					collection: 'layouts',
					search_fields: ['id'],
					value_field: '{{slug}}',
					display_fields: ['id'],
					multiple: false,
					required: false,
				},
				{
					label: 'Theme',
					name: 'theme',
					hint: 'The theme, are the CSS styles applied on the website. Selecting a theme will change the appearance of the site and all its content.',
					widget: 'select',
					required: true,
					default: 'default',
					options: [
						{
							label: 'Default',
							value: 'default',
						},
						{
							label: 'None',
							value: 'none',
						},
					],
				}
			]
		},
	]
}

export default configs
