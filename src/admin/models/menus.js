const menus	= {
	i18n: true,
	extension: 'json',
	name: 'menus',
	label: 'Menus',
	label_singular: 'Menu',
	folder: 'content/menus',
	description: 'Menus are list of "links", pointing towards pages of the website, or external webpages. Local links, should not start with "/", and external links, always start with "https://". It is possible to add menus to layouts.',
	create: true,
	delete: true,
	slug: '{{year}}-{{month}}-{{day}}-{{hour}}-{{second}}',
	editor: {
		preview: false,
	},
	fields: [
		{
			label: 'Id',
			name: 'id',
			widget: 'string',
			hint: 'The ID, is the "name" of the menu, describing where/how it is used, and referencing to this unique item.'
		},
		{
			i18n: true,
			widget: 'list',
			name: 'links',
			label: 'Menu links',
			hint: 'A menu is composed of "link", each pointing to a URL address, that will be visited by the user when the "link title" is clicked on the site.',
			fields: [
				{
					label: 'Title',
					name: 'title',
					widget: 'string',
					hint: 'The Title is the name of the menu item, as seen by the user.',
					required: true,
				},
				{
					label: 'URL',
					name: 'url',
					widget: 'string',
					hint: 'The url to the page (without the first "/", for site-internal pages), example: `courses/my-course`; it can also start with "https://", to pointe towards a URL address external to this site.',
					required: true,
				}
			]
		}
	]
}

export default menus
