import slug from '../fields/pages/slug.js'

const pages = {
	i18n: true,
	extension: 'json',
	name: 'pages',
	label: 'Pages',
	label_singular: 'Page',
	intentifier_field: 'slug',
	slug: '{{year}}-{{month}}-{{day}}-{{hour}}-{{second}}',
	description: 'A page organizes content. A website is composed of multiple pages, each of them accessible by their "URL" address, for example: "https://example.org/my-page". The "my-page", part of the URL, is called the "slug", and it should be customized to describe meaningully what is the content on this page.',
	create: true,
	delete: true,
	folder: 'content/pages',
	fields: [
		slug,
		{
			label: 'Layout',
			name: 'layout',
			widget: 'relation',
			collection: 'layouts',
			search_fields: ['id'],
			value_field: '{{slug}}',
			display_fields: ['id'],
			multiple: false,
			required: false,
			hint: 'Add a layout to this page, to be able to enforce "content groupments" or specific styling, accross multiple pages.',
		},
		{
			label: 'Sections',
			name: 'sections',
			widget: 'relation',
			collection: 'sections',
			search_fields: ['id'],
			value_field: '{{slug}}',
			display_fields: ['id'],
			multiple: true,
			required: false,
			hint: 'List which content "sections" appear on this page, and in which order.',
		}
	],
	editor: {
		preview: false,
	},
}

export default pages
