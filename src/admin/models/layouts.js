import id from '../fields/layouts/id.js'
import slots from '../fields/layouts/slots.js'

const layouts = {
	i18n: true,
	extension: 'json',
	name: 'layouts',
	label: 'Layouts',
	label_singular: 'Layout',
	identifier_field: 'id',
	description: 'Layouts organize the content on the site. It is possible to add a layout to the "config.site", to add elements (menu, sections etc.), visible at all time on all pages of the site; or to pages, for elements repeated on different pages.',
	create: true,
	delete: true,
	folder: 'content/layouts',
	fields: [
		id,
		slots,
	],
	editor: {
		preview: false,
	},
	slug: '{{year}}-{{month}}-{{day}}-{{hour}}-{{second}}',
}

export default layouts
