import id from '../fields/sections/id.js'
import blocks from '../fields/sections/blocks.js'

const pages = {
	i18n: true,
	extension: 'json',
	name: 'sections',
	label: 'Sections',
	label_singular: 'Section',
	identifier_field: 'id',
	description: 'Sections are used to list "content elements" (text, images, titles, etc.), to be added in pages or layouts. A section should be meaningfull groups, that will be positionned to compose the pages of the site with content. Add sections to pages, to see its content appear on the site.',
	create: true,
	delete: true,
	folder: 'content/sections',
	fields: [
		id,
		blocks,
	],
	editor: {
		preview: false,
	},
	slug: '{{year}}-{{month}}-{{day}}-{{hour}}-{{second}}',
}

export default pages
