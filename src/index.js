import WebSite from './components/web-site.js'
import WebPage from './components/web-page.js'
import WebSection from './components/web-section.js'
import WebMenu from './components/web-menu.js'
import WebLayout from './components/web-layout.js'

customElements.define('web-site', WebSite)
customElements.define('web-page', WebPage)
customElements.define('web-section', WebSection)
customElements.define('web-menu', WebMenu)
customElements.define('web-layout', WebLayout)

export default {
	WebSite,
	WebPage,
	WebSection,
	WebMenu,
	WebLayout,
}
